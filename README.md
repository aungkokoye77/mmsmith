### Install vendor folder via composer

~~~
$ cd ./mmsmith
$ composer install
~~~

### Yii2 built in server

~~~
$ cd ./mmsmith
$ php yii serve
~~~

Default url is : http://localhost:8080/

### Want to change port!

~~~
$ php yii serve --port=8888
~~~

**NOTES:**

Please click calculator menu from nav bar. 

Login as username: admin , password: admin.
