<?php

namespace app\controllers;

use app\models\Calculator;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;



class CalculatorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //allow to process actions
                'only' => ['index'],
                'rules' => [
                    [
                        //allow only login user for following actions
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @return string
     */
    public function actionIndex()
    {
        $model  = new Calculator();
        $result = null;

        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $result = $model->calculate();
        }

        return $this->render('index', ['model' => $model, 'result' => $result ] );
    }

}
