<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 14/05/19
 * Time: 10:40
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 *
 * @property string        $operator
 * @property integer|float $firstNumber
 * @property integer|float $secondNumber
 *
 */
class Calculator extends Model
{

    const PLUS     = 1;
    const MINUS    = 2;
    const MULTIPLY = 3;
    const DIVIDE   = 4;

    public $firstNumber;
    public $secondNumber;
    public $operator;

    /**
     * For dropdown list
     *
     * @return array
     */
    public static function getOperators()
    {
        return [
            self::PLUS     => 'Plus +',
            self::MINUS    => 'Minus -',
            self::MULTIPLY => 'Multiply *',
            self::DIVIDE   => 'Divide /',
        ];
    }


    /**
     * For dropdown array list
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'firstNumber'  => Yii::t('app', 'First Number'),
            'secondNumber' => Yii::t('app', 'Second Number'),
            'operator'     => Yii::t('app', 'Operator'),
        ];
    }

    /**
     * the validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['firstNumber', 'secondNumber', 'operator'], 'required'],
            [['firstNumber', 'secondNumber'], 'number'],
            [['operator'], 'in', 'range' => [self::PLUS, self::MINUS, self::MULTIPLY, self::DIVIDE]],
            [['secondNumber'], 'validateDivide']
        ];
    }

    /**
     * check zero value for divide.
     *
     * @param $attribute
     * @param $params
     */
    public function validateDivide($attribute, $params)
    {

        if (!$this->hasErrors()) {

            if ($this->operator == self::DIVIDE && $this->secondNumber == 0) {

                $this->addError($attribute, 'Cannot divide by zero.');

            }
        }

    }

    /**
     *
     * @return float|int
     */
    public function calculate()
    {

        switch($this->operator){

            case self::PLUS :
                return $this->firstNumber + $this->secondNumber;
            case self::MINUS :
                return $this->firstNumber - $this->secondNumber;
            case self::MULTIPLY :
                return $this->firstNumber * $this->secondNumber;
            default :
                return $this->firstNumber / $this->secondNumber;

        }

    }
}