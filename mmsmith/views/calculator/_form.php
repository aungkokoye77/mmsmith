<?php
/**
 * Created by PhpStorm.
 * User: ak1
 * Date: 14/05/19
 * Time: 10:36
 */

use app\models\Calculator;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Calculator */
/* @var $result integer|float|null */

?>

<div class="row">

    <div class="calculator-form col-lg-6">

        <?php if ($result !== null) : ?>
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Result : </strong> <?= Yii::$app->formatter->asDecimal($result)?>
            </div>
        <?php endif; ?>

        <?php $form = ActiveForm::begin(['options' => ['autoComplete' => 'off']]); ?>

        <?= $form->field($model, 'firstNumber')
                 ->textInput() ?>

        <?= $form->field($model, 'operator')
                 ->dropDownList(Calculator::getOperators(), ['prompt' => 'Select Operator ...']) ?>

        <?= $form->field($model, 'secondNumber')
                 ->textInput(['rows' => 8]) ?>

        <div class="form-group">
            <?= Html::submitButton('Calculate', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>


