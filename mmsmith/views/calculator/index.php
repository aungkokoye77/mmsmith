<?php

use app\models\Calculator;

/* @var $this yii\web\View */
/* @var $model Calculator */
/* @var $result integer|float| null */

$this->title = 'Calculator';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="calculator-index ">

    <div class="panel panel-default">

        <div class="panel-heading">
            <?= $this->title ?>
        </div>

        <div class="panel-body">

            <?= $this->render('_form' , ['model' => $model, 'result' => $result]) ?>

        </div>
    </div>

</div>
